﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class ProductDao
    {
        OnlineShopDbContent db = null;
        public ProductDao()
        {
            db = new OnlineShopDbContent();
        }

        public IEnumerable<Product> SearchProduct(string searching)
        {
            IQueryable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(searching))
            {
                model = db.Products.Where(x => x.Name.Contains(searching));
            }

            return model.OrderByDescending(x => x.CreateDate).ToList();

        }
        public long Insert(Product entity)
        {
            entity.CreateDate = DateTime.Now;
            db.Products.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }


        public List<Product> listProductByCategory(long id, ref int totalRecord, int pageIndex=1, int pageSize =10)
        {
            totalRecord = db.Products.Where(x => x.CategoryID == id).Count();
            //var model = from a in db.Products
            //            join b in db.ProductCategories
            //            on a.CategoryID equals b.ID
            //            where a.CategoryID == id
            //            select new ProductViewModel()
            //            {
            //                CateMetaTitle = b.MetaTitle,
            //                CateName = b.Name,
            //                CreateDate = a.CreateDate,
            //                ID = a.ID,
            //                Images = a.Image,
            //                Name = a.Name,
            //                MetaTitle = a.MetaTitle,
            //                Price = a.Price
            //            };
            //model.OrderByDescending(x=>x.CreateDate).Skip((pageIndex - 1)* pageSize).Take(pageSize);
            var model = db.Products.Where(x => x.CategoryID == id);
            model.OrderByDescending(x => x.CreateDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return model.ToList();
        }

        public List<Product> ListNewProduct(int top)
        {
            return db.Products.OrderByDescending(x => x.CreateDate).Take(top).ToList();
        }
        public List<Product> NewProduct()
        {
            return db.Products.OrderByDescending(x => x.CreateDate).ToList();
        }

        public List<Product> ListFeatureProduct(int top)
        {
            return db.Products.Where(x => x.TopHot != null && x.TopHot > DateTime.Now).OrderByDescending(x => x.CreateDate).Take(top).ToList();
        }

        public List<Product> FeatureProduct()
        {
            return db.Products.Where(x => x.TopHot != null && x.TopHot > DateTime.Now).OrderByDescending(x => x.CreateDate).ToList();
        }

        public List<Product> listRelateProduct(long id)
        {
            var product = db.Products.Find(id);
            return db.Products.Where(x => x.ID != id && x.CategoryID == product.CategoryID).ToList();
        }

        public IEnumerable<Product> ListAll(ref int totalRecord, int pageIndex = 1, int pageSize = 10)
        {
            totalRecord = db.Products.Count();
            var model = db.Products.OrderByDescending(x => x.CreateDate).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return model;
        }

        public List<Product> Lists()
        {
            return db.Products.ToList();
        }


        public List<Product> ListAllActive()
        {
            return db.Products.Where(x => x.Status == true).ToList();
        }


        public bool Update(Product entity)
        {
            try
            {
                var product = db.Products.Find(entity.ID);

                product.Name = entity.Name;
                product.Price = entity.Price;
                product.Status = entity.Status;
                product.Image = entity.Image;
                product.IncludeVAT = entity.IncludeVAT;
                product.MetaTitle = entity.MetaTitle;
                product.PromotionPrice = entity.PromotionPrice;
                product.Quantity = entity.Quantity;
                product.TopHot = entity.TopHot;
                product.Detail = entity.Detail;
                product.Description = entity.Description;
                product.Code = entity.Code;
                product.CategoryID = entity.CategoryID;

                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Deleted(long id)
        {
            try
            {
                var product = db.Products.Find(id);

                db.Products.Remove(product);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Product GetByProductName(string productname)
        {
            return db.Products.SingleOrDefault(x => x.Name == productname);
        }

        public Product ViewDetail(long id)
        {
            return db.Products.Find(id);
        }
    }
}
