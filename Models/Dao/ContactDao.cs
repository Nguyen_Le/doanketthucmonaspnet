﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class ContactDao
    {
        OnlineShopDbContent db = null;

        public ContactDao()
        {
            db = new OnlineShopDbContent();
        }

        public long Insert(Contact entity)
        {
            db.Contacts.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public IEnumerable<Contact> ListAll()
        {
            return db.Contacts.ToList();
        }

        public Contact ViewDetail(long id)
        {
            return db.Contacts.Find(id);
        }

        public bool Update(Contact entity)
        {
            try
            {
                var contact = db.Contacts.Find(entity.ID);
                contact.Status = entity.Status;
                contact.Content = entity.Content;

                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Deleted(long id)
        {
            try
            {
                var contact = db.Contacts.Find(id);
                db.Contacts.Remove(contact);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
