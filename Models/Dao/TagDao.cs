﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class TagDao
    {
        OnlineShopDbContent db = null;
        public TagDao()
        {
            db = new OnlineShopDbContent();
        }

        public IEnumerable<Tag> ListAll()
        {
            return db.Tags.ToList();
        }

        public string Insert(Tag entity)
        {
            db.Tags.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }
    }
}
