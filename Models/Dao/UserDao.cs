﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;
using PagedList;

namespace Models.Dao
{
    public class UserDao
    {
        OnlineShopDbContent db = null;
        public UserDao()
        {
            db = new OnlineShopDbContent();
        }
        public long Insert(User entity)
        {
            entity.CreateDate = DateTime.Now;
            db.Users.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }
       
        public IEnumerable<User> ListAll()
        {

            return db.Users.ToList();
        }

        public bool Update(User entity)
        {
            try {
                var user = db.Users.Find(entity.ID);
                user.Name = entity.Name;

                if (!string.IsNullOrEmpty(entity.Password))
                {
                    user.Password = entity.Password;
                }

                user.Address = entity.Address;
                user.Email = entity.Email;
                user.ModifedBy = entity.ModifedBy;
                user.ModifedDate = DateTime.Now;
                user.Status = entity.Status;
                db.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
            

        }
        public User GetByUserName(string username)
        {
            return db.Users.SingleOrDefault(x => x.UserName == username);
        }

        public User ViewDetail(int id)
        {
            return db.Users.Find(id);
        }

        public int Login(string username,string password)
        {
            var result = db.Users.SingleOrDefault(x => x.UserName == username);
            if (result == null)
            {
                return 0;
            }
            else
            {
                if(result.Status == false)
                {
                    return -1;
                }
                else
                {
                    if(result.Password == password)
                    {
                        return 1;
                    }
                    else
                    {
                        return -2;
                    }
                }
            }
        }

        public bool Deleted(int id)
        {
            try
            {
                var user = db.Users.Find(id);
                db.Users.Remove(user);
                db.SaveChanges();
                return true;
            }catch(Exception)
            {
                return false;
            }
            
        }

        public bool ChangeStatus(long id)
        {
            var user = db.Users.Find(id);
            user.Status = !user.Status;
            db.SaveChanges();
            return user.Status;
        }

    }
}
