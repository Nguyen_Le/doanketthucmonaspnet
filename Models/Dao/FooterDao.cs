﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class FooterDao
    {
        OnlineShopDbContent db = null;

        public FooterDao()
        {
            db = new OnlineShopDbContent();
        }

        public Footer getFooter()
        {
            return db.Footers.SingleOrDefault(x => x.Status == true);
        }
    }
}
