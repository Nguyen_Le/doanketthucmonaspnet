﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class MenuDao
    {
        OnlineShopDbContent db = null;

        public MenuDao()
        {
            db = new OnlineShopDbContent();
        }

        public List<Menu> ListByGroupID(int groupId)
        {
            return db.Menus.Where(x => x.TypeID == groupId && x.Status == true).OrderBy(x=>x.DisplayOrder).ToList();
        }

        public long Insert(Menu entity)
        {
            db.Menus.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public IEnumerable<Menu> ListAll()
        {
            return db.Menus.ToList();
        }

        public bool Update(Menu entity)
        {
            try
            {
                var menu = db.Menus.Find(entity.ID);
                menu.Link = entity.Link;
                menu.Status = entity.Status;
                menu.Target = entity.Target;
                menu.Text = entity.Text;
                menu.TypeID = entity.TypeID;
                menu.DisplayOrder = entity.DisplayOrder;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Deleted(long id)
        {
            try
            {
                var menu = db.Menus.Find(id);
                db.Menus.Remove(menu);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Menu GetByID(long id)
        {
            return db.Menus.Find(id);
        }
    }
}
