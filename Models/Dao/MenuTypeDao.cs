﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class MenuTypeDao
    {
        OnlineShopDbContent db = null;
        public MenuTypeDao()
        {
            db = new OnlineShopDbContent();
        }

        public IEnumerable<MenuType> ListAll()
        {
            return db.MenuTypes.ToList();
        }
    }
}
