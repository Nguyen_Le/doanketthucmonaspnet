﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class CategoryDao
    {
        OnlineShopDbContent db = null;
        public CategoryDao()
        {
            db = new OnlineShopDbContent();
        }
        public long Insert(Category category)
        {
            category.CreateDate = DateTime.Now;
            db.Categories.Add(category);
            db.SaveChanges();
            return category.ID;
        }

        public IEnumerable<Category> ListAll()
        {
            return db.Categories.ToList();
        }

        public List<Category> ListAllActive()
        {
            return db.Categories.Where(x => x.Status == true).ToList();
        }
        public Category ViewDetail(long id)
        {
            return db.Categories.Find(id);
        }
        public ProductCategory ViewDetailCategory(long id)
        {
            return db.ProductCategories.Find(id);
        }
        public bool Update(Category entity)
        {
            try
            {
                var category = db.Categories.Find(entity.ID);
                category.Name = entity.Name;
                category.Status = entity.Status;
                category.MetaTitle = entity.MetaTitle;
                category.ShowOnHome = entity.ShowOnHome;
                category.DisplayOrder = entity.DisplayOrder;
                
                db.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }

        }

        public bool Deleted(long id)
        {
            try
            {
                var category = db.Categories.Find(id);
                db.Categories.Remove(category);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
            
        }
    }
}
