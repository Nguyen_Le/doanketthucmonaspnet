﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class OrderDao
    {
        OnlineShopDbContent db = null;
        public OrderDao()
        {
            db = new OnlineShopDbContent();
        }
        public long Insert(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return order.ID;
        }

        public List<Order> listAll()
        {
            return db.Orders.ToList();
        }

        public Order ViewDetail(long id)
        {
            return db.Orders.Find(id);
        }

        public bool Update(Order entity)
        {
            try
            {
                var order = db.Orders.Find(entity.ID);
                order.Status = entity.Status;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
