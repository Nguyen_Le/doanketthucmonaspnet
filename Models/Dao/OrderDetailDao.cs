﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class OrderDetailDao
    {
        OnlineShopDbContent db = null;
        public OrderDetailDao()
        {
            db = new OnlineShopDbContent();
        }
        public bool Insert(OrderDetail detail)
        {
            try
            {
                db.OrderDetails.Add(detail);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;

            }
        }

        public List<OrderDetail> listAllByOrderID(long id)
        {

            return db.OrderDetails.Where(x => x.OrderID == id).ToList();
        }
    }
}
