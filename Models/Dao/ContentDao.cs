﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.EF;

namespace Models.Dao
{
    
    public class ContentDao
    {
        OnlineShopDbContent db = null;

        public ContentDao()
        {
            db = new OnlineShopDbContent();
        }
        public long Insert(Content content)
        {
            db.Contents.Add(content);
            db.SaveChanges();
            return content.ID;
        }

        public IEnumerable<Content> ListAll()
        {
            return db.Contents.ToList();
        }

        public bool Update(Content entity)
        {
            try
            {
                var content = db.Contents.Find(entity.ID);
                content.Name = entity.Name;
                content.MetaTitle = entity.MetaTitle;
                content.Image = entity.Image;
                content.Detail = entity.Detail;
                content.CategoryID = entity.CategoryID;
                content.Status = entity.Status;
                content.Description = entity.Description;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Content ViewDetail(long id)
        {
            return db.Contents.Find(id);
        }

        public bool Deleted(int id)
        {
            try
            {
                var content = db.Contents.Find(id);
                db.Contents.Remove(content);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }


    }

    
}
