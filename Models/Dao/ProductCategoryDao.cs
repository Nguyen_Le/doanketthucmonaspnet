﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dao
{
    public class ProductCategoryDao
    {
        OnlineShopDbContent db = null;
        public ProductCategoryDao()
        {
            db = new OnlineShopDbContent();
        }

        public List<ProductCategory> ListAll()
        {
            return db.ProductCategories.Where(x => x.Status == true).OrderBy(x=>x.DisplayOrder).ToList();
        }

        public List<ProductCategory> ListAllCategory()
        {
            return db.ProductCategories.ToList();
        }

        public ProductCategory ViewDetail(long id)
        {
            return db.ProductCategories.Find(id);
        }
        public long Insert(ProductCategory entity)
        {
            db.ProductCategories.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public bool Update(ProductCategory entity)
        {
            try
            {
                var productCategory = db.ProductCategories.Find(entity.ID);
                productCategory.Name = entity.Name;
                productCategory.ParentID = entity.ParentID;
                productCategory.SeoTitle = entity.SeoTitle;
                productCategory.ShowOnHome = entity.ShowOnHome;
                productCategory.Status = entity.Status;
                productCategory.DisplayOrder = entity.DisplayOrder;
                productCategory.MetaTitle = entity.MetaTitle;

                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Deleted(long id)
        {
            try
            {
                var productCategory = db.ProductCategories.Find(id);
                db.ProductCategories.Remove(productCategory);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        
    }
}
