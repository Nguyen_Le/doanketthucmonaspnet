﻿namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Category")]
    public partial class Category
    {
        public long ID { get; set; }

        [StringLength(250)]
        [Display(Name = "Loại tin tức")]
        public string Name { get; set; }

        [StringLength(250)]
        public string MetaTitle { get; set; }
        [Display(Name = "Thuộc loại tin tức")]
        public long? ParentID { get; set; }
        [Display(Name = "Vị trí hiển thị")]
        public int? DisplayOrder { get; set; }

        [StringLength(250)]
        public string SeoTitle { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        public DateTime? ModifedDate { get; set; }

        [StringLength(50)]
        public string ModifedBy { get; set; }

        [StringLength(250)]
        public string MetaKeyword { get; set; }

        [StringLength(250)]
        public string MetaDescription { get; set; }
        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }

        public bool ShowOnHome { get; set; }
    }
}
