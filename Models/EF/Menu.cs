﻿namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Menu
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Text { get; set; }

        [StringLength(50)]
        public string Link { get; set; }
        [Display(Name = "Vị trí hiển thị")]
        public int? DisplayOrder { get; set; }

        [StringLength(50)]
        public string Target { get; set; }
        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }
        [Display(Name = "Vị trí")]
        public int? TypeID { get; set; }
    }
}
