﻿var user = {
    init: () => {
        user.registerEvents();
    },
    registerEvents: () => {
        $('.btn-active').off('click').on('click', (e) => {
            e.preventDefault();
            var btn = $('.btn-active');
            var id = btn.data('id');

            $.ajax({
                url: "/Admin/User/ChangeStatus",
                data: { id: id},
                dataType: "json",
                type: "POST",
                success: response => {
                    if (response.status == true) {
                        btn.text('Actived');
                    } else {
                        btn.text('Locked');
                    }
                }
            });
        });

    }
}
user.init();

//function changeStatus() {
//    let idUser = document.getElementById('btnActive').value;
//    console.log(idUser);
//    $.ajax({
//        url: "/Admin/User/ChangeStatus",
//        data: { id: idUser },
//        dataType: "json",
//        type: "POST",
//        success: response => {
//            if (response.status == true) {
//                $(this).text('Actived');
//            } else {
//                $(this).text('Locked');
//            }
//        }
//    })
//}