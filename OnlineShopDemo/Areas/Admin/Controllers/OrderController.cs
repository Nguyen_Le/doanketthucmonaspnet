﻿using Models.Dao;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Areas.Admin.Controllers
{
    public class OrderController : BaseController
    {
        // GET: Admin/Order
        public ActionResult Index()
        {

            var orders = new OrderDao().listAll();
            return View(orders);
        }
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var details = new OrderDetailDao().listAllByOrderID(id);
            var orders = new OrderDao().ViewDetail(id);

            ViewBag.OrderDetail = details;
            return View(orders);
        }
        [HttpPost]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                var dao = new OrderDao();
                bool result = dao.Update(order);
                if (result)
                {
                    return RedirectToAction("Index", "Order");
                }
                else
                {
                    ModelState.AddModelError("", "Update fail, please try again!!");
                }
            }
            return View("Index");
        }
    }
}