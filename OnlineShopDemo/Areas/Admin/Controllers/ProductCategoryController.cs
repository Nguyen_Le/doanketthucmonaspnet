﻿using Models.Dao;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Areas.Admin.Controllers
{
    public class ProductCategoryController : Controller
    {
        // GET: Admin/CategoryProduct
        public ActionResult Index()
        {
            var productCategory = new ProductCategoryDao().ListAllCategory();
            return View(productCategory);
        }
        [HttpGet]
        public ActionResult Create()
        {
            SetViewBag();
            return View();
        }

        [HttpGet]
        public ActionResult Edit(long id)
        {
            var productCategory = new ProductCategoryDao().ViewDetail(id);
            SetViewBag(productCategory.ParentID);
            return View(productCategory);
        }
        [HttpPost]
        public ActionResult Create(ProductCategory model)
        {
            if (ModelState.IsValid)
            {
                var dao = new ProductCategoryDao();
                model.CreateDate = DateTime.Now;
                long id = dao.Insert(model);
                if (id > 0)
                {
                    return RedirectToAction("Index", "ProductCategory");
                }
                else
                {
                    ModelState.AddModelError("", "Add new content success!!");
                }
            }

            SetViewBag();
            return View("Index");
        }

        [HttpPost]
        public ActionResult Edit(ProductCategory model)
        {
            if (ModelState.IsValid)
            {
                var dao = new ProductCategoryDao();
                model.ModifedDate = DateTime.Now;
                bool result = dao.Update(model);
                if (result)
                {
                    return RedirectToAction("Index", "ProductCategory");
                }
                else
                {
                    ModelState.AddModelError("", "Update false, please try again!!!");
                }
            }
            SetViewBag(model.ParentID);
            return View("Index");

        }

        public void SetViewBag(long? selectedID = null)
        {
            var dao = new ProductCategoryDao();
            ViewBag.ParentID = new SelectList(dao.ListAllCategory(), "ID", "Name", selectedID);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new ProductCategoryDao().Deleted(id);
            return RedirectToAction("Index");
        }
    }
}