﻿using Models.Dao;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Areas.Admin.Controllers
{
    public class ContentController : BaseController
    {
        // GET: Admin/Content
        public ActionResult Index()
        {
            var dao = new ContentDao();
            var model = dao.ListAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            SetViewBag();
            return View();
        }

        [HttpGet]
        public ActionResult Edit(long id)
        {
            var content = new ContentDao().ViewDetail(id);
            SetViewBag(content.CategoryID);
            return View(content);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Content model)
        {
            if (ModelState.IsValid)
            {

                var dao = new ContentDao();
                model.CreateDate = DateTime.Now;
                long id = dao.Insert(model);
                if (!string.IsNullOrEmpty(model.CreateDate.ToString()))
                {
                    model.CreateDate = DateTime.Now;
                }
                if (id > 0)
                {
                    return RedirectToAction("Index", "Content");
                }
                else
                {
                    ModelState.AddModelError("", "Add new content success!!");
                }
            }

            SetViewBag();
            return View("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Content content)
        {
            if (ModelState.IsValid)
            {
                var dao = new ContentDao();
                content.ModifedDate = DateTime.Now;
                bool result = dao.Update(content);
                if (result)
                {
                    return RedirectToAction("Index", "Content");
                }
                else
                {
                    ModelState.AddModelError("", "Update false, please try again!!!");
                }
            }
            SetViewBag(content.CategoryID);
            return View("Index");

        }
        public void SetViewBag(long? selectedID = null)
        {
            var dao = new CategoryDao();
            ViewBag.CategoryID = new SelectList(dao.ListAllActive(),"ID","Name",selectedID);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new ContentDao().Deleted(id);
            return RedirectToAction("Index");
        }
    }
}