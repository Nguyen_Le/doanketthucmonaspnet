﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineShopDemo.Areas.Admin.Data
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Please enter Username!!")]
        public string UserName { set; get; }

        [Required(ErrorMessage ="Pleae enter Password!!")]
        public string Password { set; get; }

        public bool RememberMe { set; get; }
    }
}