﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OnlineShopDemo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Product",
                url: "san-pham",
                defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Payment",
                url: "thanh-toan",
                defaults: new { controller = "Cart", action = "Payment", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Hoan thanh",
                url: "hoan-thanh",
                defaults: new { controller = "Cart", action = "Success", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Feedback",
                url: "lien-he",
                defaults: new { controller = "Feedback", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "About",
                url: "gioi-thieu",
                defaults: new { controller = "About", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );
            routes.MapRoute(
                name: "Content",
                url: "tin-tuc",
                defaults: new { controller = "Content", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );
            routes.MapRoute(
                name: "Gio hang",
                url: "gio-hang",
                defaults: new { controller = "Cart", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "San pham moi",
                url: "san-pham-moi",
                defaults: new { controller = "Product", action = "NewProduct", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "San pham noi bat",
                url: "san-pham-noi-bat",
                defaults: new { controller = "Product", action = "FeatureProduct", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );
            routes.MapRoute(
                name: "Add Cart",
                url: "them-gio-hang",
                defaults: new { controller = "Cart", action = "AddItem", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );
            routes.MapRoute(
                name: "Product Detail",
                url: "chi-tiet/{metatitle}-{id}",
                defaults: new { controller = "Product", action = "Detail", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Product Category",
                url: "san-pham/{metatitle}-{id}",
                defaults: new { controller = "Product", action = "Category", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Detail Content",
                url: "tin-tuc/{metatitle}-{id}",
                defaults: new { controller = "Content", action = "Detail", id = UrlParameter.Optional },
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new[] { "OnlineShopDemo.Controllers" }
            );

            
        }
    }
}
