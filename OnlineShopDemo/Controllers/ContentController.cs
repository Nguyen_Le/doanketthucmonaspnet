﻿using Models.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Controllers
{
    public class ContentController : Controller
    {
        // GET: Content
        public ActionResult Index()
        {
            var contents = new ContentDao().ListAll();
            return View(contents);
        }

        public ActionResult Detail(long id)
        {
            var content = new ContentDao().ViewDetail(id);
            return View(content);
        }
    }
}