﻿using Models.Dao;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(int page = 1, int pageSize = 6)
        {
            int totalRecord = 0;
            var products = new ProductDao().ListAll(ref totalRecord, page, pageSize);
            ViewBag.Total = totalRecord;
            ViewBag.Page = page;

            int maxPage = 5;
            int totalPage = 0;

            totalPage = (int)Math.Ceiling((double)(totalRecord / pageSize));
            ViewBag.TotalPage = totalPage;
            ViewBag.MaxPage = maxPage;
            ViewBag.First = 1;
            ViewBag.Last = totalPage;
            ViewBag.Next = page + 1;
            ViewBag.Prev = page - 1;

            return View(products);
        }
        public ActionResult ListSearch(string seaching)
        {
            var productDao = new ProductDao();
            var model = productDao.SearchProduct(seaching);
            ViewBag.Products = seaching;
            ViewBag.Seaching = seaching;
            return View(model);
        }

        [ChildActionOnly]
        public PartialViewResult ProductCategory()
        {
            var model = new ProductCategoryDao().ListAll();

            return PartialView(model);
        }

        public ActionResult Category(long id, int page = 1, int pageSize = 10)
        {
            var category = new ProductCategoryDao().ViewDetail(id);
            ViewBag.Category = category;
            int totalRecord = 0;
            var model = new ProductDao().listProductByCategory(id, ref totalRecord, page, pageSize);

            ViewBag.Total = totalRecord;
            ViewBag.Page = page;

            int maxPage = 5;
            int totalPage = 0;

            totalPage = (int)Math.Ceiling((double)(totalRecord / pageSize));
            ViewBag.TotalPage = totalPage;
            ViewBag.MaxPage = maxPage;
            ViewBag.First = 1;
            ViewBag.Last = totalPage;
            ViewBag.Next = page + 1;
            ViewBag.Prev = page - 1;

            return View(model);
        }

        public ActionResult Detail(long id)
        {
            var dao = new ProductDao();
            var product = dao.ViewDetail(id);
            ViewBag.ListFeatureProduct = dao.listRelateProduct(id);
            return View(product);
        }

        public ActionResult NewProduct()
        {
            var dao = new ProductDao();
            var product = dao.NewProduct();

            return View(product);
        }

        public ActionResult FeatureProduct()
        {
            var dao = new ProductDao();
            var product = dao.FeatureProduct();

            return View(product);
        }

        
    }
}