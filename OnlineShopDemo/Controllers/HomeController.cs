﻿using Models.Dao;
using OnlineShopDemo.Common;
using OnlineShopDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShopDemo.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index(string seaching)
        {
            //ViewBag.slides = new SlideDao().ListAll();
            var productDao = new ProductDao();
            ViewBag.ProductNew = productDao.ListNewProduct(10);
            ViewBag.ProductCategorys = new ProductCategoryDao().ListAll();
            ViewBag.ListFeatureProduct = productDao.ListFeatureProduct(10);
            
            return View();
        }

        
        [ChildActionOnly]
        public ActionResult MainMenu()
        {
            var model = new MenuDao().ListByGroupID(1);
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Footer()
        {
            var model = new FooterDao().getFooter();
            return PartialView(model);
        }

        [ChildActionOnly]
        public PartialViewResult HeaderCart()
        {
            var cart = Session[CommonConstants.CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return PartialView(list);
        }
    }
}